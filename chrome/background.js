var notifications = [];

function getBase64Image(url, background) {
    var img = null;
    if (url) {
        img = new Image();
        img.src = url;
    }

    var canvas = document.createElement('canvas');
    var ctx = canvas.getContext('2d');
    canvas.height = img ? img.height : 48;
    canvas.width = img ? img.width : 48;
    ctx.fillStyle = background;
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    if (img) {
        ctx.drawImage(img, 0, 0);
    }

    return canvas.toDataURL();
}

function showNotifyClick(event) {
    event.preventDefault();
    window.open(this.data.value, '_blank');
    deleteNotification(this);
    this.close();
}

function showNotify(info) {
    if (Notification.permission !== 'granted') {
        Notification.requestPermission(function (permission) {
            // If the user accepts, let's create a notification
            if (permission === "granted") {
                showNotify(info);
            }
        });
    }
    var notifs = [];
    info.forEach(function (inf) {
        if (inf.active) {
            var options = {
                // icon: inf.image,
                icon: getBase64Image(inf.image, inf.background),
                requireInteraction: true
            };
            inf.elements.forEach(function (elem) {
                options.body = elem.name;
                options.data = {
                    value: elem.value
                };
                var n = new Notification(inf.title, options);
                n.onclick = showNotifyClick;
                n.onclose = function (event) {
                    event.preventDefault();
                    deleteNotification(this);
                };
                notifs.push(n);
            });
        }
    });
    notifications = notifs.concat(notifications);
    return notifs;
}

function deleteNotification(notify) {
    notifications = notifications.filter(function (n) {
        return n.data.value !== notify.data.value;
    });
}

function closeAllNotifications() {
    notifications.forEach(function (notify) {
        notify.close();
    });
}

chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
    var result = null;
    switch (request.action) {
        case 'show':
            result = showNotify(request.ndata);
            break;
        case 'closeall':
            result = closeAllNotifications();
            break;
        default:
    }
    sendResponse({result: result});
});
