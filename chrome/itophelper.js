'use strict';

function GM_setValue(key, data) {
    localStorage.setItem(key, JSON.stringify(data));
}

function GM_getValue(key, def) {
    var data = localStorage.getItem(key);
    return data ? JSON.parse(data) : def;
}

function getOld() {
    return GM_getValue('saved_items', []);
}

function saveElements(data) {
    if (!data) {
        data = getActual();
    }
    GM_setValue('saved_items', data);
}

function saveCheck(id, value) {
    GM_setValue('check_' + id, value);
}

function isChecked(id, def) {
    if (def === undefined) {
        def = false;
    }
    return GM_getValue('check_' + id, def);
}

function saveColor(id, value) {
    GM_setValue('color_' + id, value);
}

function getColor(id) {
    return GM_getValue('color_' + id, '');
}

function getDashBoards() {
    return $('.dashboard');
}

function getItems(dashboard) {
    var result = [];
    // var image;
    $('.dashlet', dashboard).each(function (index, dash) {
        var isactive = $('.activate-selector:checked', dash).length > 0;
        if ($('.main_header', dash).length === 0 && isactive) {
            var title = $('h1', dash).text();
            var block = $('.display_block', dash)[0];
            var id = block.id;
            var elements = [];
            var istable = $('.listContainer', block).length > 0;
            var background = $('.selector-color', dash).val();
            $('table.listResults tbody tr', block).each(function (trindex, tr) {
                var elem = {};
                if (istable) {
                    elem.name = $('td:nth-child(1)', tr).text();
                    elem.value = $('td:nth-child(1) a', tr)[0].href;
                } else {
                    var n = $('td:nth-child(2) a', tr);
                    elem.value = n[0].href;
                    elem.name = $('td:nth-child(1)', tr).text() + ' (' + parseInt(n.text()) + ')';
                }
                var trcls = $(tr).attr('class');
                if (trcls === 'orange') {
                    elem.name = '+ ' + elem.name;
                } else if (trcls === 'orange_even') {
                    elem.name = '++ ' + elem.name;
                } else if (trcls === 'red_even') {
                    elem.name = '+++ ' + elem.name;
                }
                elements.push(elem);
            });

            var image = null;
            if ($(dash).parent().find('.main_header img').length) {
                image = $(dash).parent().find('.main_header img').attr('src');
                try {
                    if (image.indexOf('./') === 0) {
                        image = window.location.protocol + '//' + window.location.host + image.slice(1);
                        image = encodeURI(image);
                    }
                } catch (e) {
                    console.error('ITOP Manager error', e);
                }
            }
            result.push({
                id: id,
                title: title,
                image: image,
                background: background,
                istable: istable,
                active: $('input.activate-selector[type=checkbox]:checked', dash).length > 0,
                elements: elements
            });
        }
    });
    return result;
}

function getActual() {
    var actual = [];
    getDashBoards().each(function (index, d) {
        var dashinfo = getItems(d);
        dashinfo.forEach(function (info) {
            actual.push(info);
        });
    });
    return actual;
}

function findBlock(id, arr) {
    return arr.filter(function (info) {
        return info.id === id;
    }).pop();
}

function compare(a, b) {
    return JSON.stringify(a) === JSON.stringify(b);
}

function searchDiff(old, actual) {
    actual.forEach(function (act) {
        // find in old
        var diff = null;
        var o = findBlock(act.id, old);
        if (!o) {
            diff = Object.assign({}, act);
        } else if (!compare(act, o)) {
            diff = Object.assign({}, act);
            diff.elements = diff.elements
                .filter(function (elem) {
                    var keep = true;
                    o.elements.forEach(function (oelem) {
                        if (o.istable) {
                            keep = keep ? !compare(elem, oelem) : keep;
                        } else {
                            keep = keep ? elem.count > oelem.count : keep;
                        }
                    });
                    return keep;
                });
        }
        if (diff && diff.elements && diff.elements.length) {
            notifyChange(diff);
        }
    });
}

var maxtimeout = 500;
var notifies = [];
var timer = null;

function notifyChange(info) {
    if (!info) {
        return;
    }
    if (timer) {
        clearTimeout(timer);
    }
    if (!findBlock(info.id, notifies)) {
        notifies.push(info);
    }
    timer = setTimeout(function () {
        showNotify(notifies);
        notifies = [];
    }, maxtimeout);
}

function showNotify(notify) {
    chrome.runtime.sendMessage({action: 'show', ndata: notify});
}

function showDecorations() {
    var select = $('<select>')
        .attr('class', 'selector-color')
        .on('change', changeColor);

    var check = $('<input>')
        .attr({
            type: 'checkbox',
            class: 'activate-selector'
        })
        .on('change', changeCheck);

    var label = $('<label>')
        .text('Notifications');

    select.append(new Option('No color', 'none', true));
    var colors = {
        aqua: "#00ffff",
        // azure: "#f0ffff",
        // beige: "#f5f5dc",
        // black: "#000000",
        blue: "#0000ff",
        brown: "#a52a2a",
        cyan: "#00ffff",
        darkblue: "#00008b",
        darkcyan: "#008b8b",
        darkgrey: "#a9a9a9",
        // darkgreen: "#006400",
        // darkkhaki: "#bdb76b",
        darkmagenta: "#8b008b",
        // darkolivegreen: "#556b2f",
        darkorange: "#ff8c00",
        // darkorchid: "#9932cc",
        // darkred: "#8b0000",
        // darksalmon: "#e9967a",
        // darkviolet: "#9400d3",
        fuchsia: "#ff00ff",
        gold: "#ffd700",
        green: "#008000",
        // indigo: "#4b0082",
        khaki: "#f0e68c",
        lightblue: "#add8e6",
        // lightcyan: "#e0ffff",
        // lightgreen: "#90ee90",
        lightgrey: "#d3d3d3",
        // lightpink: "#ffb6c1",
        // lightyellow: "#ffffe0",
        // lime: "#00ff00",
        // magenta: "#ff00ff",
        // maroon: "#800000",
        // navy: "#000080",
        // olive: "#808000",
        // orange: "#ffa500",
        // pink: "#ffc0cb",
        // purple: "#800080",
        // violet: "#800080",
        red: "#ff0000",
        silver: "#c0c0c0",
        white: "#ffffff",
        yellow: "#ffff00"
    };
    Object.keys(colors).forEach(function (color) {
        select.append(new Option(color, colors[color]));
    });

    var blocks = $('div.display_block[id*=block_]:visible').parent();
    blocks.each(function (i, block) {
        var id = $(block).find('> div.display_block')[0].id;
        var sel = select.clone(true, true);
        var c = check.clone(true, true);
        var l = label.clone(true, true);
        var ch = isChecked(id);
        var color = getColor(id);
        c.attr({
            checked: ch,
            dataid: id
        });
        sel.attr({
            dataid: id
        });
        sel.val(color);
        if (!ch) {
            sel.hide();
        }
        $(block).find('h1')
            .after(sel)
            .after($(l).append($(c)));

        changeColor(sel);
    });

    var closeall = $('<button>').text('Close all notifications');
    closeall.on('click', closeAll);

    var title = $('.dashboard_contents > h1');
    title.after(closeall);
}

function closeAll() {
    chrome.runtime.sendMessage({action: 'closeall'});
}

function changeColor(selector) {
    if (selector.hasOwnProperty('type')) {
        selector = $(this);
    }
    var color = selector.is(':visible') ? selector.val() : 'none';
    saveColor(selector.attr('dataid'), color);
    selector.parent()
        .find('h1')
        .attr('style', 'background:' + color);
}

function changeCheck() {
    var id = this.getAttribute('dataid');
    var sel = $(this).parents('.dashlet-content').find('select[dataid=' + id + ']');
    saveCheck(id, this.checked);
    if (sel.length) {
        if (this.checked) {
            sel.show();
        } else {
            sel.hide();
        }
        changeColor(sel);
    }
    saveElements();
}

function init() {
    showDecorations();
    var actual = getActual();
    var old = getOld();
    if (old && old.length) {
        searchDiff(old, actual);
    }
    saveElements(actual);
}

$(document).ready(init);
